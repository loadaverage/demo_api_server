const debug   = require('debug')('entrypoint');
const config  = require('./config');
const url     = require('url');
const uuid    = require('uuid/v1');
const http    = require('http');
const Redis   = require('ioredis');
const redis   = new Redis(config.redis);
const helpers = require('./helpers');
const future_message = helpers.future_message;
const json_dumps     = helpers.json_dumps;
const send_reply     = helpers.send_reply;
const todo           = {};

redis.on('ready', () => {

  const server = http.createServer((req, res) => {

    const params   = url.parse(req.url, true);
    const api_path = config.api.path;

    if (params.pathname == api_path) {
      let epoch   = params.query.epoch;
      let message = params.query.message;
      let est_eta;
      // Add uuid to the duplicates messages, so we can serve them
      message = todo.hasOwnProperty(message) ? message + '::' + uuid() : message;

      debug('new request; message: %s, epoch: %s', message, epoch);

      const attempt = future_message(epoch, message, redis, todo, false);
      attempt
        .catch((epoch_error) => {
          send_reply(res, 422, json_dumps({
            status: "error",
            message: `epoch: (${epoch}) must be set and should be greater than current timestamp: ${Date.now()}`
          }));
        })
        .then((delta) => {
          est_eta = delta;
          return redis.zadd('events', epoch, message);
        })
        .catch((zadd_err) => {
          send_reply(res, 500, json_dumps({
            status: 'error',
            message: zadd_err.message
          }));
        })
        .then((result) => {
          send_reply(res, 201, json_dumps({
            status: 'created',
            eta: est_eta
          }));
        })
        .catch((e) => {
          send_reply(res, 500, json_dumps({
            status: "error",
            message: e
          }));
        });

    } else {
      send_reply(res, 404, json_dumps({
        status: "error",
        message: "wrong arguments or path"
      }));
    }
  });
  server.listen(config.api.port);
});

redis.on('connect', () => {

  redis.zrangebyscore('events', '0', '+inf', 'withscores')
    .then((result) => {
      debug('pulled messages: %d', result.length / 2);

      let epoch, message;

      for (let el = 0, next = 1; el < result.length; el++, next += 2) {
        if (next <= result.length) {
          message = result[next - 1];
          epoch = result[next];
          future_message(epoch, message, redis, todo, true);
        }
      }

    })
    .catch((e) => {
      debug('got an error while pulling events');
    });

});
