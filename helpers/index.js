/**
 * Helpers
 */

const debug = require('debug')('helpers');

/**
 * Prints a message in the stdout and remove members from the sorted set
 * @function
 * @param {Number} epoch Unix timestamp
 * @param {String} message A message to send
 * @param {Object} redis Redis connection instance
 * @param {Object} todo A local storage for duplicate members
 * @param {Boolean} outdated Send even if the message timestamp is in the past
 * @returns {Promise} Promise with time delta
 *   timestamp is in the past and $outdated is not set
 */

function future_message(epoch, message, redis, todo, outdated) {
  return new Promise((resolve, reject) => {
    if (Date.now() < epoch || outdated) {
      todo[message] = epoch;
      const delta = epoch - Date.now();
      let output = message.split('::');
      output = output.length >= 1 ? output[0] : '';
      setTimeout(() => {
        console.log(output);
        debug('message printed: %s', output);

        redis.zrem('events', message)
          .then((result) => {
            delete todo[message];
            debug('member was removed: %s', message);
          })
          .catch((e) => {
            debug('error during removal member: %s', message);
          });
      }, delta);
      debug('new message queued, eta in %s ms', delta);
      resolve(delta);
    } else {
      reject();
    }
  });
}

/**
 * Safe JSON stringifier
 * @function
 * @param {Object} Object to stringify
 * @returns {String}
 */

function json_dumps(obj) {
  let str;
  try {
    str = JSON.stringify(obj);
  } catch (e) {
    str = '';
  } finally {
    return str;
  }
}

/**
 * Generates reply with custom status code and message body
 * @function
 * @param {Object} res Server's response object
 * @param {Number} status HTTP Status code
 * @param {String} message HTTP message body
 * @returns {Object} ServerResponse
 */

function send_reply(res, status, message) {
  const header_content = {
    code: status,
    header: {
      'content-type': 'application/json'
    }
  };
  res.writeHead(header_content.code, header_content.header);
  res.end(message);
}

module.exports.future_message = future_message;
module.exports.json_dumps = json_dumps;
module.exports.send_reply = send_reply;

