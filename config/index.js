/**
 * Redis and API server configuration
 */

const main = {
  redis: {
    port: process.env['REDIS_PORT'],
    host: process.env['REDIS_SERVER'],
    password: process.env['REDIS_PASSWORD']
  },
  api: {
    path: process.env['API_PATH'],
    port: process.env['API_PORT']
  }
};

module.exports = main;
