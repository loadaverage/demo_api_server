# Demo API messaging server with Redis as a persistent storage

---

#### howto:

>The only requirements is Docker and docker-compose.
>Don't forget to adjust api_server's source volume and listening port in the docker-compose.yaml to your configuration

```bash
git clone https://bitbucket.org/loadaverage/demo_api_server && cd demo_api_server && docker-compose up
```

 >Push a message to the queue

```bash
curl -i http://127.0.0.1/v1/api/echoAtTime?epoch=1538593319339&message=new_message_16799
```
Where `epoch` is the UNIX timestamp in milliseconds and `message` is the message that would be (hopefully) printed to the server's console.

>Example:
```bash
http://127.0.0.1/v1/api/echoAtTime?epoch=1538593507447&message=new_message_12082
HTTP/1.1 201 Created
content-type: application/json
Date: Wed, 03 Oct 2018 19:05:07 GMT
Connection: keep-alive
Transfer-Encoding: chunked

{"status":"created","eta":16954}
```
>Where status can be one of: `created` or `error` in the case of server error.
>And eta is the Estimated Time of Arrival in milliseconds.
